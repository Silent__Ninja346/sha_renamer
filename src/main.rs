use pbr::ProgressBar;
use sha2::{Digest, Sha256};
use std::env;
use std::error::Error;
use std::fs;
use std::path::PathBuf;
use std::process;

struct Config {
    dirname: String,
    change_names: bool,
}

impl Config {
    fn new(mut args: std::env::Args) -> Result<Config, &'static str> {
        args.next();

        let dirname = match args.next() {
            Some(arg) => arg,
            None => return Err("Didn't get a dirname"),
        };

        let change_names = match args.next() {
            Some(arg) => {
                if arg != "-y" {
                    false
                } else {
                    true
                }
            }
            None => false,
        };

        Ok(Config {
            dirname,
            change_names,
        })
    }
}

fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let c = fs::read_dir(&config.dirname)?
        .filter_map(|entry| entry.ok())
        .count();
    let mut pb = ProgressBar::new(c.try_into().unwrap());
    for entry in fs::read_dir(config.dirname)?.filter_map(|entry| entry.ok()) {
        let old_path = entry.path();
        if old_path.is_file() {
            let old_path_sha =
                format!("{:x}", Sha256::new().chain(fs::read(&old_path)?).finalize());
            let old_path_extension = old_path.extension().unwrap_or_default();

            if let Some(_) = old_path
                .file_stem()
                .filter(|&stem| stem != old_path_sha.as_str())
            {
                let mut new_path = PathBuf::from(&old_path);
                new_path.set_file_name(old_path_sha);
                new_path.set_extension(old_path_extension);

                println!("Old Path: {:?}\nNew Path: {:?}\n", &old_path, &new_path);
                if config.change_names {
                    fs::rename(old_path, new_path)?;
                }
            }
        }
        pb.inc();
    }
    pb.finish_print("done");
    Ok(())
}

fn main() {
    let config = Config::new(env::args()).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments: {}", err);
        process::exit(1);
    });

    if let Err(e) = run(config) {
        eprintln!("Error: {}", e);
        process::exit(1);
    }
}
